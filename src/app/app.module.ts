import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { UsuarioComponent } from './usuario/usuario.component';
import { AgmCoreModule } from '@agm/core';
import { TokenService } from './token.service';
import { CarrouselComponent } from './carrousel/carrousel.component';
import { CarouselService } from './carousel.service';


@NgModule({
   declarations: [
      AppComponent,
      UsuarioComponent,
      CarrouselComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      AgmCoreModule.forRoot({
         apiKey:'AIzaSyCA1m8-w56s4sKrb8AIkqF5vL8Oc8ST8go',
         libraries: ['geometry', 'places']
      }),
      NgbModule
      
   ],
   providers: [TokenService,CarouselService],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
