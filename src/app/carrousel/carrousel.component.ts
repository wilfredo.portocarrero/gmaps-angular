import { Component, OnInit, ViewChild } from '@angular/core';
import {CarouselService} from '../carousel.service';
import { NgbSlideEvent, NgbSlideEventSource, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';

@Component({
  selector: 'app-carrousel',
  templateUrl: './carrousel.component.html',
  styleUrls: ['./carrousel.component.css']
})
export class CarrouselComponent implements OnInit {
  
  @ViewChild('mycarousel', {static : true}) carousel: NgbCarousel;
  constructor(protected carouselservice : CarouselService) { }

  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
  fotosCarousel:any[]=[];
  


  ngOnInit() {
    

    this.carouselservice.fotos();

    this.fotosCarousel =JSON.parse(localStorage.getItem('fotosCarousel'));
    console.log(this.fotosCarousel);
    //this.carousel.pause();
    
   
   
    
  
  
  }

  


  elegir(item){
    
  }

  goToSlide(slide) {
    this.carousel.select(slide);
  }

}
