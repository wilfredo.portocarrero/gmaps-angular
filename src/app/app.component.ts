import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { TokenService } from './token.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  texto: string = "google maps";
  lat: number = -23.8779431;
  lng: number = -49.8046873;
  zoom: number = 18;
  //token: any[] = [];
  token: string;
  valortoken: string;
  @ViewChild('search', { static: false })
  public searchElementRef: ElementRef;
  geoCoder: google.maps.Geocoder;
  latitude: number;
  longitude: number;
  address: string;
  establecimiento: string;
  autocomplete: string;
  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, protected tokenservice: TokenService) { }
  //asdasdasdasdasdasdasdasdasdasdasd
  ngOnInit() {


    //post token extraer latitud y long de establecimiento

    this.tokenservice.Token();
    this.tokenservice.establecimiento();

    this.establecimiento = localStorage.getItem('establecimiento');






    /*
          this.tokenservice.getToken().subscribe(
            (data)=>{
              this.token=data['sToken'];
              localStorage.setItem('token', this.token);
             alert(localStorage.getItem('token'));
            },
            (error)=>{
              console.log(error);
            }
          )
         */

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"],
        componentRestrictions: { country: "pe" }
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();

          this.longitude = place.geometry.location.lng();
          
          this.zoom = 18;
          this.getAddress(this.latitude, this.longitude);
        });
      });
    });

  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        var latitud_establecimiento = parseFloat(localStorage.getItem('latitud'));
        var longitud_establecimiento = parseFloat(localStorage.getItem('longitud'));
        this.latitude = latitud_establecimiento;
        this.longitude = longitud_establecimiento;
        this.zoom = 18;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }
  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 18;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }

  //asdasdasdasdasdasdasdasdasdasd

  buscarmapa(latitudenvio: any, longitudenvio: any) {
    var latitud = parseFloat(latitudenvio);
    var longitud = parseFloat(longitudenvio);

    alert(latitud + " , " + longitud);
    this.lat = latitud;
    this.lng = longitud;
    this.zoom = 18;
  }

  mapReady() {

  }

  llamar() {
    this.lat = -12.0958883;
    this.lng = -77.0306644;
  }






}