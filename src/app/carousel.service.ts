import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CarouselService {
  token : string;

  constructor(private http : HttpClient) { }

  getfotos() {

    var headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${localStorage.getItem('token')}`)
    }
    console.log(localStorage.getItem('token'));
    return this.http.get("https://us-central1-devthepoint-47564.cloudfunctions.net/Home?Carrusel=true", headers);

  }
  fotos() {
    this.getfotos().subscribe(
      (data) => {
        console.log(data);
        localStorage.setItem('fotosCarousel',JSON.stringify(data['listObjsDto']));

       // console.log(localStorage.getItem('fotosCarousel'));
        //this.latEst=data['sLatitud'];
        /*localStorage.setItem('latitud', data['objDto']['sLatitud']);
        localStorage.setItem('longitud', data['objDto']['sLongitud']);
        localStorage.setItem('establecimiento', data['objDto']['sNombre']);
      
        console.log(data);
        console.log(data['objDto']['sLatitud']);
        console.log(data['objDto']['sLongitud']);*/
      
      }
    )
  }
}
