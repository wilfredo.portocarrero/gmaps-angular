import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarrouselComponent } from './carrousel/carrousel.component';
import { AppComponent } from './app.component';


const routes: Routes = [{
  path:'carrousel',
  component: CarrouselComponent

},
{
  path:'',
component:AppComponent
},
{
  path:'**',
component:AppComponent
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
